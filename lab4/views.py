from django.shortcuts import render
import datetime

# Create your views here.
def lab1(request):
    return render(request, "lab1.html")

def aboutpage(request):
    return render(request, "aboutpage.html")

def contact(request):
    return render(request, "contact.html")  

def time(request, hour=0):
    time = datetime.datetime.now() + datetime.timedelta(hours=1 * hour)
    return render(request, "time.html", {'time':time})


