from django.contrib import admin
from django.urls import path
from . import views

app_name='lab5'
urlpatterns = [
    path('create', views.create_jadwal),
    path('jadwal', views.jadwal, name='jadwal'),
    path('details', views.details, name='details'),
    path('delete', views.delete)
]
