from . import models
from django import forms
import datetime

class JadwalForm(forms.ModelForm):
        nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Nama Mata Kuliah",
                }))
        tempat = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Ruangan",
                }))
        kategori = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Dosen Pengajar",
                }))
        thnA = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Tahun Ajaran",
                }))
        sks = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Jumlah SKS",
                }))
        descrip = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "jadwalfields full",
                "required" : True,
                "placeholder":"Deskripsi Mata Kuliah",
                }))
        tanggal = forms.DateField(widget=forms.SelectDateWidget(attrs={
                "class" : "datefield jadwalfields",
                "required" : True,
                }), initial=datetime.datetime.now() )
        waktu = forms.TimeField(widget=forms.TimeInput(attrs={
                "class" : "datefield jadwalfields",
                "placeholder":"00:00",
                "required" : True,
                }), initial=datetime.datetime.now() ) 

        class Meta:
                model = models.Jadwal
                fields = ["nama_kegiatan", "kategori", "thnA", "sks", "descrip", "tempat", "tanggal", "waktu"]