# Generated by Django 2.2 on 2019-10-01 18:33

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('lab5', '0003_auto_20191001_1832'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwal',
            name='tanggal',
            field=models.DateField(default=datetime.datetime(2019, 10, 1, 18, 33, 50, 112266, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='jadwal',
            name='waktu',
            field=models.TimeField(default=datetime.datetime(2019, 10, 1, 18, 33, 50, 112313, tzinfo=utc)),
        ),
    ]
