from django.urls import path 
from . import views

app_name = 'lab1'

urlpatterns = [
    path('', views.lab1, name='lab1'),
]